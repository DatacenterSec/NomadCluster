resource "digitalocean_record" "hello_service_domain" {
  domain = "${var.domain}"
  type   = "A"
  ttl    = 60
  name   = "hello"
  value  = "${digitalocean_droplet.loadbalancer.ipv4_address}"
}

resource "digitalocean_record" "world_service_domain" {
  domain = "${var.domain}"
  type   = "A"
  ttl    = 60
  name   = "world"
  value  = "${digitalocean_droplet.loadbalancer.ipv4_address}"
}
