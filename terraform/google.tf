provider "google" {
  credentials = "${file("~/.gcloud/account.json")}"
  project     = "adrienne-devops"
  region      = "us-west1"
}
