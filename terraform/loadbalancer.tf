resource "digitalocean_droplet" "loadbalancer" {
  image              = "${data.digitalocean_image.loadbalancer.image}"
  size               = "s-2vcpu-2gb"
  region             = "sfo1"
  name               = "loadbalancer"
  ssh_keys           = ["${var.ssh_key}"]
  private_networking = true
  tags               = ["loadbalancer"]
}

resource "digitalocean_record" "loadbalancer_fqdn" {
  domain = "${var.domain}"
  type   = "A"
  ttl    = 60
  name   = "${digitalocean_droplet.loadbalancer.name}"
  value  = "${digitalocean_droplet.loadbalancer.ipv4_address}"
}

data "digitalocean_image" "loadbalancer" {
  name = "loadbalancer-${var.commit_id}"
}
