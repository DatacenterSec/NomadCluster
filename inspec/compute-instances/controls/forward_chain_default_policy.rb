control 'iptables-forward-chain' do
    impact 0.8
    title 'Ensure FORWARD chain is in ACCEPT mode'
    desc 'Check that FORWARD chain has default policy ACCEPT.'

    describe iptables(table: 'filter', chain: 'FORWARD') do
        it { should have_rule('-P FORWARD ACCEPT') }
    end
end
