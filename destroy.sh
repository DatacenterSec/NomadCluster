#!/bin/bash

set -e

# Determine commit hash
if [ -z ${CI_COMMIT_SHA} ];
then
    COMMIT_HASH=$(git rev-parse HEAD)
else
    COMMIT_HASH=${CI_COMMIT_SHA}
fi

cd terraform
terraform destroy -auto-approve \
    -var "username=$(whoami)" \
    -var "hostname=$(hostname)" \
    -var "commit_id=${COMMIT_HASH}"
