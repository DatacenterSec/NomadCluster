#!/bin/bash

curl -Os https://releases.hashicorp.com/consul-template/0.19.5/consul-template_0.19.5_linux_amd64.tgz
tar -zxf consul-template_0.19.5_linux_amd64.tgz --directory /usr/bin
sync
